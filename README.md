# sunseeker

## Videos

- https://www.youtube.com/watch?v=BxA7qwmY9mg
- https://www.youtube.com/watch?v=00NTVf1dwPU
- https://www.youtube.com/watch?v=81C4IfONt3o
- https://www.youtube.com/watch?v=OBM5T5_kgdI
- https://www.youtube.com/watch?v=j6nQZggypF4 
- https://www.youtube.com/watch?v=cM7t1Mpu7s4
- https://www.youtube.com/watch?v=giGRrODKJSE
- https://www.youtube.com/watch?v=kNVaIqmKUoI
- https://www.youtube.com/watch?v=bXEyCf1P0UU

## Reading

- http://www.st.com/content/ccc/resource/technical/document/datasheet/group3/b2/1e/33/77/c6/92/47/6b/DM00279086/files/DM00279086.pdf/jcr:content/translations/en.DM00279086.pdf Page 35
- https://learn.sparkfun.com/tutorials/pull-up-resistors
- https://blog.adafruit.com/2012/01/24/choosing-the-right-crystal-and-caps-for-your-design/
- https://www.allaboutcircuits.com/textbook/direct-current/chpt-13/series-and-parallel-capacitors/
- https://electronics.stackexchange.com/questions/36167/understanding-the-avcc-pin-wiring-on-arduinoleonardo-low-pass-filter
- https://arduino.stackexchange.com/questions/49182/atmega-avcc-pin-low-pass-filter
- https://www.dronetrest.com/t/what-in-an-lc-filter-and-do-i-need-one/1381/2
- https://www.reddit.com/r/AskElectronics/comments/923cro/why_do_we_need_a_diode_in_parallel_with_a_resistor/
- https://www.reddit.com/r/AskElectronics/comments/92b1e6/are_vcc_and_gnd_both_power_inputs/?utm_content=full_comments&utm_medium=message&utm_source=reddit&utm_name=frontpage
- https://electronut.in/bootloader-atmega32u4/
- https://forum.arduino.cc/index.php?topic=158717.0
- http://www.ti.com/lit/ml/slup239/slup239.pdf
- https://www.digikey.com/en/articles/techzone/2015/feb/design-trade-offs-when-selecting-a-high-frequency-switching-regulator
- https://electronics.stackexchange.com/questions/186086/two-voltage-regulators-in-series-vs-in-parallel#186091
- http://www.analog.com/media/en/technical-documentation/data-sheets/ADP2302_2303.pdf
- https://en.wikipedia.org/wiki/Equivalent_series_resistance#Capacitors

## Notes

- `CL - CS = (C1 * C2) / (C1 + C2)`, where `2 < CS < 5 (pF)`. If `C1 = C2`, then `C1 = 2 * (CL - CS)`.
